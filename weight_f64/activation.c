#include <float.h>
#include <math.h>
#include <string.h>
#include "activation.h"

const ActivationPair ActivationList[ACTIVATION_COUNT] = {
    {&identity_a, &identity_d},
    {&sigmoid_a, &sigmoid_d},
    {&tanh_a, &tanh_d},
    {&relu_a, &relu_d},
    {&clipped_relu_a, &clipped_relu_d},
    {&gelu_a, &gelu_d},
    {&softplus_a, &softplus_d},
    {&elu_a, &elu_d},
    {&leaky_relu_a, &leaky_relu_d},
    {&silu_a, &silu_d},
    {&mish_a, &mish_d},
    {&gaussian_a, &gaussian_d},
};

void identity_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    memcpy(outputs, inputs, size * sizeof(double));
}

void identity_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    (void)inputs;

    for (size_t i = 0; i < size; ++i)
        outputs[i] = 1.0;
}

void sigmoid_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = 1.0 / (1.0 + exp(-inputs[i]));
}

void sigmoid_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    sigmoid_a(inputs, outputs, size);

    for (size_t i = 0; i < size; ++i)
        outputs[i] *= 1.0 - outputs[i];
}

void tanh_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = tanh(inputs[i]);
}

void tanh_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    tanh_a(inputs, outputs, size);

    for (size_t i = 0; i < size; ++i)
        outputs[i] = 1.0 - outputs[i] * outputs[i];
}

void relu_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = inputs[i] < 0.0 ? 0.0 : inputs[i];
}

void relu_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = (inputs[i] > 0.0) ? 1.0 : 0.0;
}

void clipped_relu_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = inputs[i] < 0.0 ? 0.0 : inputs[i] > 1.0 ? 1.0 : inputs[i];
}

void clipped_relu_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = (inputs[i] > 0.0 && inputs[i] < 1.0) ? 1.0 : 0.0;
}

void gelu_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = 0.5 * inputs[i] * (1.0 + erf(inputs[i] / M_SQRT2));
}

void gelu_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = (0.5 * inputs[i] + 0.5) * (1.0 + erf(inputs[i] / M_SQRT2));
}

void softplus_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = log(1.0 + exp(inputs[i]));
}

void softplus_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    sigmoid_a(inputs, outputs, size);
}

void elu_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = inputs[i] >= 0.0 ? inputs[i] : exp(inputs[i]) - 1.0;
}

void elu_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = inputs[i] >= 0.0 ? 1.0 : exp(inputs[i]);
}

void leaky_relu_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = inputs[i] >= 0.0 ? inputs[i] : inputs[i] * 0.01;
}

void leaky_relu_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = inputs[i] >= 0.0 ? 1.0 : 0.01;
}

void silu_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    sigmoid_a(inputs, outputs, size);

    for (size_t i = 0; i < size; ++i)
        outputs[i] *= inputs[i];
}

void silu_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        double e = exp(-inputs[i]);

        outputs[i] = (1.0 + e + inputs[i] * e) / ((1.0 + e) * (1.0 + e));
    }
}

void mish_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = inputs[i] * tanh(log(1.0 + exp(inputs[i])));
}

void mish_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        double x = inputs[i];
        double e = exp(x);

        outputs[i] = (e * (4.0 * e * e + e * e * e + 4.0 * (1.0 + x) + e * (6.0 + 4 * x))) / pow(2.0 + 2.0 * e + e * e, 2.0);
    }
}

void gaussian_a(const double *restrict inputs, double *restrict outputs, size_t size)
{
    for (size_t i = 0; i < size; ++i)
        outputs[i] = exp(-inputs[i] * inputs[i]);
}

void gaussian_d(const double *restrict inputs, double *restrict outputs, size_t size)
{
    gaussian_a(inputs, outputs, size);

    for (size_t i = 0; i < size; ++i)
        outputs[i] *= -2.0 * inputs[i];
}
