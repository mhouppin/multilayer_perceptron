#include <float.h>
#include <math.h>
#include <string.h>
#include "weight.h"

int integer_load(FILE *fp, uint32_t *u);
int integer_save(FILE *fp, uint32_t u);

int wload(FILE *fp, weight_t *w)
{
    // Floating point weights are saved in ieee754 format to files,
    // using 11 exponent bits and 52 mantissa bits, in little-endian.
    unsigned char buffer[8];

    if (fread(buffer, 1, 8, fp) != 8)
        return -1;

    int sign = buffer[7] & 0x80 ? -1 : 1;
    int exponent = ((buffer[7] & 0x7F) << 4) | ((buffer[6] & 0xF0) >> 4);

    double bitValue = 0.5;
    double mantissa = 0.0;
    unsigned int byteIndex = 6;
    unsigned char mask = 0x08;

    for (int i = 0; i < 52; ++i)
    {
        if (buffer[byteIndex] & mask)
            mantissa += bitValue;

        bitValue *= 0.5;
        mask >>= 1;

        if (!mask)
        {
            mask = 0x80;
            --byteIndex;
        }
    }

    // Handle the zero case now
    if (exponent == 0 && mantissa == 0.0)
    {
        *w = 0.0;
        return 0;
    }

    // "Raw" exponent lies in [0, 2047], "real" exponent lies in [-1023, 1024]
    exponent -= 1023;

    // Handle infs and NaNs
    if (exponent == 1024)
    {
        if (mantissa != 0.0)
            *w = NAN;
        else
            *w = sign == 1 ? INFINITY : -INFINITY;

        return 0;
    }

    // Handle denormalized numbers
    if (exponent == -1023)
    {
        if (mantissa == 0.0)
        {
            *w = 0.0;
            return 0;
        }

        ++exponent;

        while (mantissa < 1.0)
        {
            mantissa += mantissa;
            --exponent;
        }

        *w = ldexp(mantissa, exponent) * sign;
        return 0;
    }

    *w = ldexp(1.0 + mantissa, exponent) * sign;
    return 0;
}

int wsave(FILE *fp, weight_t w)
{
    uint32_t hi, lo;

    // Zero (we don't care about sign)
    if (w == 0.0)
    {
        hi = lo = 0;
        goto write_weight;
    }

    // Infinity
    if (w > DBL_MAX)
    {
        hi = (uint32_t)2047 << 20;
        lo = 0;
        goto write_weight;
    }

    // -Infinity
    if (w < -DBL_MAX)
    {
        hi = (uint32_t)2047 << 20;
        hi |= (uint32_t)1 << 31;
        lo = 0;
        goto write_weight;
    }

    // NaN
    if (isnan(w))
    {
        hi = (uint32_t)2047 << 20;
        lo = 1;
        goto write_weight;
    }

    // Get the sign
    uint32_t sign = (w < 0) ? 1 : 0;
    w = (w < 0) ? -w : w;

    // Compute the exponent
    int exponent = 0;

    while (w >= 2.0)
    {
        w *= 0.5;
        ++exponent;
    }

    while (w < 1.0)
    {
        w += w;
        --exponent;
    }

    // Check for denormalized numbers
    if (exponent < -1022)
    {
        while (exponent < -1022)
        {
            w *= 0.5;
            ++exponent;
        }

        --exponent;
    }

    // Out of range (which could mean that the system uses more bits for
    // exponents than we do), in this case set the number to infinity
    else if (exponent > 1023)
    {
        hi = (uint32_t)2047 << 20;
        hi |= sign << 31;
        lo = 0;
        goto write_weight;
    }
    else
        w -= 1.0;

    double mantissaRaw = w * (((uint64_t)1 << 52) + 0.5);
    exponent += 1023;

    uint32_t hiMantissa = mantissaRaw / 4294967296.0;

    hi = (sign << 31) | (exponent << 20) | hiMantissa;
    lo = mantissaRaw - hiMantissa * 4294967296.0;

write_weight:
    integer_save(fp, lo);
    return integer_save(fp, hi);
}
