NAME := libnetwork.a

SOURCES := dataset.c network.c training.c

EXTRA_DIR ?= weight_f64

SOURCES += $(EXTRA_DIR)/weight.c $(EXTRA_DIR)/activation.c $(EXTRA_DIR)/matrix.c

OBJECTS := $(SOURCES:.c=.o)
DEPENDS := $(SOURCES:.c=.d)

CFLAGS += -Wall -Wextra -Werror -std=gnu11 -MMD -I$(EXTRA_DIR) -I. -O3 -march=native

all: $(NAME)

$(NAME): $(OBJECTS)
	ar rcs $(NAME) $(OBJECTS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

-include $(DEPENDS)

clean:
	rm -f $(OBJECTS) $(DEPENDS)

fclean: clean
	rm -f $(NAME)

re:
	$(MAKE) fclean
	+$(MAKE) all
